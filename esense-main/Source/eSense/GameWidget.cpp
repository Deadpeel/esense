// Fill out your copyright notice in the Description page of Project Settings.


#include "GameWidget.h"

#include "CanvasPanelSlot.h"
#include "GameSettingsSubsystem.h"
#include "Blueprint/WidgetLayoutLibrary.h"
#include "Components/CanvasPanel.h"
#include "Kismet/GameplayStatics.h"

void UGameWidget::NativeConstruct()
{
	Super::NativeConstruct();
	UGameSettingsSubsystem* MySettings = GetGameInstance()->GetSubsystem<UGameSettingsSubsystem>();
	for (int i=0;i<=MySettings->NumberOFShapes;i++)
	{
		GameBloc.Add(CreateWidget<UGameBloc>(GetWorld(),WidgetClass));//add widget

		Canvas->AddChildToCanvas(GameBloc[i]);
	}
	//UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), YourParticleSystem, Location, Rotation);
}

void UGameWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

	UGameSettingsSubsystem* MySettings = GetGameInstance()->GetSubsystem<UGameSettingsSubsystem>();
	for (int i=0;i<GameBloc.Num();i++)
	{
		GameBloc[i]->SetImagine(MySettings->SelectedShape);
	}
	
	for (int i = 0 ; i <Canvas->GetChildrenCount();i++)
	{
		FVector2D movement=FVector2D(300*InDeltaTime,300*InDeltaTime);
		UCanvasPanelSlot* CanvasSlot = Cast<UCanvasPanelSlot>(Canvas->GetChildAt(i)->Slot);
		FVector2D newposition = CanvasSlot->GetPosition()+movement;
		CanvasSlot->SetPosition(newposition);
	}
}