// Fill out your copyright notice in the Description page of Project Settings.


#include "MyGameInstanceSubsystem.h"

#include "HttpModule.h"
#include "IHttpResponse.h"
#include "JsonObject.h"
#include "JsonReader.h"
#include "JsonSerializer.h"
#include "JsonUtilities/Public/JsonObjectConverter.h"


void UMyGameInstanceSubsystem::LoginWithCredentials(const FString& Username, const FString& Password)
{
	FHttpRequestPtr Request = FHttpModule::Get().CreateRequest();
	Request->OnProcessRequestComplete().BindUObject(this, &UMyGameInstanceSubsystem::OnResponseReceived);
	Request->SetURL("https://esense.ro/api/auth/login");
	Request->SetVerb("POST");
	Request->SetHeader("Content-Type", TEXT("application/json"));

	FLoginPost InfoSent;
	InfoSent.email= Username;
	InfoSent.password= Password;
	FString jsonBody;
	FJsonObjectConverter::UStructToJsonObjectString(InfoSent, jsonBody);
	Request->SetContentAsString(jsonBody);
	
	Request->ProcessRequest();
}

void UMyGameInstanceSubsystem::UpdateStudents()
{
	FString tokenTest = FString::Printf(TEXT("Bearer %s"), *Info.token);
	FHttpRequestPtr Request = FHttpModule::Get().CreateRequest();
	Request->OnProcessRequestComplete().BindUObject(this, &UMyGameInstanceSubsystem::OnStudentsReceived);
	Request->SetURL("https://esense.ro/api/lessons/students");
	Request->SetVerb("GET");
	Request->SetHeader("Content-Type", TEXT("application/json"));
	Request->SetHeader("Authorization", tokenTest);
	Request->ProcessRequest();
}

const FMyResult& UMyGameInstanceSubsystem::GetUserInfo() const
{
	return Info;
}

const FStudentResult& UMyGameInstanceSubsystem::GetStudentInfo() const
{
	return StudentInfo;
}

void UMyGameInstanceSubsystem::OnResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response,bool bWasSuccessful)
{
	UE_LOG(LogTemp, Warning, TEXT("Get response"));
	TSharedPtr<FJsonObject> JsonObject;
    
	TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(Response->GetContentAsString());

	if(FJsonSerializer::Deserialize(Reader, JsonObject))
	{
		if (FJsonObjectConverter::JsonObjectToUStruct(JsonObject.ToSharedRef(), &Info))
		{
		}
		UE_LOG(LogTemp, Warning, TEXT("ResponseCode: %s"), *LexToString(Response->GetResponseCode()));
	}

	const bool LoggingSuccess = EHttpResponseCodes::IsOk(Response->GetResponseCode());
	IReceivedStuff.Broadcast(LoggingSuccess);

}

void UMyGameInstanceSubsystem::OnStudentsReceived(FHttpRequestPtr Request, FHttpResponsePtr Response,bool bWasSuccessful)
{
	//UE_LOG(LogTemp, Warning, TEXT("Students Recieved: %s"), *LexToString(bWasSuccessful));
	FString JsonRaw = FString::Printf(TEXT("{\"users\": %s}"), *Response->GetContentAsString());
	TSharedPtr<FJsonObject> JsonObject;
    
	TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(JsonRaw);

	if(FJsonSerializer::Deserialize(Reader, JsonObject))
	{
		if (FJsonObjectConverter::JsonObjectToUStruct(JsonObject.ToSharedRef(), &StudentInfo))
		{
			UE_LOG(LogTemp, Warning, TEXT("Parsing success: %s"), *StudentInfo.users[0].name);
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("Parsing failed."));
		}
		const bool StudentList = EHttpResponseCodes::IsOk(Response->GetResponseCode());
		StudentsReceived.Broadcast();
		    UE_LOG(LogTemp, Warning, TEXT("ResponseCode: %s"), *Response->GetContentAsString());
	}
}

