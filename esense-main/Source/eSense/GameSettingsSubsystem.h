﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "IHttpRequest.h"
#include "SlateBrush.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "GameSettingsSubsystem.generated.h"


UCLASS()
class ESENSE_API UGameSettingsSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()

	public:
	FLinearColor SelectedColor;
	FLinearColor BackgroundColor;
	FSlateBrush SelectedShape;
	int32 NumberOFShapes=5;

	virtual void Initialize(FSubsystemCollectionBase& Collection) override;
};