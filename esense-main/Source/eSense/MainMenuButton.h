// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Button.h"
#include "ScaleBox.h"
#include "TextBlock.h"
#include "Blueprint/UserWidget.h"
#include "MainMenuButton.generated.h"

/**
 * 
 */
UCLASS()
class ESENSE_API UMainMenuButton : public UUserWidget
{
	GENERATED_BODY()

public:
	
	UPROPERTY(meta=(BindWidget))
	UButton* StudentButton = nullptr;

	UPROPERTY(meta=(BindWidget))
	UTextBlock* StudentText = nullptr;

	UPROPERTY(meta=(BindWidget))
	UScaleBox* StudentScale = nullptr;
	
};
