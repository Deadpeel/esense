// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;

public class eSense : ModuleRules
{
	public eSense(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
	
		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore","JsonUtilities","Json","HTTP" , "SLATECORE" });

		PrivateDependencyModuleNames.AddRange(new string[] {  });
		
	}
}
