// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "eSenseGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class ESENSE_API AeSenseGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(EditAnywhere)
	TSubclassOf<UUserWidget> WidgetClass;

	virtual void BeginPlay() override;
};
