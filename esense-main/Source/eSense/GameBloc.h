// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Image.h"
#include "Blueprint/UserWidget.h"
#include "GameBloc.generated.h"

/**
 * 
 */
UCLASS()
class ESENSE_API UGameBloc : public UUserWidget
{
	GENERATED_BODY()
	
	UPROPERTY(meta=(BindWidget))
	UImage* Bloc=nullptr;

public:
	void SetImagine(FSlateBrush Brush);
};
