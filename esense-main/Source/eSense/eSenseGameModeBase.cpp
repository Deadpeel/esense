// Fill out your copyright notice in the Description page of Project Settings.


#include "eSenseGameModeBase.h"

#include "LoginUI.h"
#include "MainMenuUI.h"
#include "GameMenuUI.h"
#include "GamesUI.h"
#include "UserWidget.h"
#include "GameWidget.h"
#include "Kismet/GameplayStatics.h"

void AeSenseGameModeBase::BeginPlay()
{
	Super::BeginPlay();
	
	if(IsValid(WidgetClass))
	{
		
		UUserWidget* WidgetInstance = CreateWidget(GetWorld(),WidgetClass);
		WidgetInstance->AddToViewport();
		
		APlayerController* Player = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		Player->bShowMouseCursor = true;
	}
}
