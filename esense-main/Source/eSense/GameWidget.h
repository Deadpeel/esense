// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CanvasPanel.h"
#include "GameBloc.h"
#include "Image.h"
#include "WrapBox.h"
#include "Blueprint/UserWidget.h"
#include "GameWidget.generated.h"

/**
 * 
 */
UCLASS()
class ESENSE_API UGameWidget : public UUserWidget
{
	GENERATED_BODY()

	virtual void NativeConstruct() override;
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime);

	UPROPERTY(EditAnywhere)
	TSubclassOf<UUserWidget> WidgetClass;

	UPROPERTY(meta=(BindWidget))
	UCanvasPanel* Canvas;
	
	
	UPROPERTY()
	TArray<UGameBloc*> GameBloc;
	
};
