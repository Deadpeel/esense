// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Button.h"
#include "CanvasPanel.h"
#include "Image.h"
#include "TextBlock.h"
#include "Blueprint/UserWidget.h"
#include "ColorButton.generated.h"

/**
 * 
 */
UCLASS()
class ESENSE_API UColorButton : public UUserWidget
{
	GENERATED_BODY()

public:
	
	DECLARE_MULTICAST_DELEGATE_TwoParams(FColorReceived, UButton* /*ButtonReceived*/, FLinearColor /*ColorReceived*/);

	FColorReceived ColorReceived;

	DECLARE_MULTICAST_DELEGATE_TwoParams(FTextureReceived, UButton* /*ButtonReceived*/, FSlateBrush /*TextureReceived*/);
	
	FTextureReceived TextureReceived;
	
	UPROPERTY(meta=(BindWidget))
	UButton* InnerButton;

	UPROPERTY(meta=(BindWidget)) 
	UImage* InnerImage;        

	UPROPERTY(EditAnywhere)
	FSlateBrush Brush;
	
	virtual void NativeConstruct() override;
	virtual void NativePreConstruct() override; 

	UFUNCTION()
	void OnInnerButtonClicked();

	UFUNCTION()
	void OnInnerTextureButtonClicked();
};
