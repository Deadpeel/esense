// Fill out your copyright notice in the Description page of Project Settings.


#include "MainMenuUI.h"
#include "eSenseGameModeBase.h"
#include "MainMenuButton.h"
#include "MyGameInstanceSubsystem.h"
#include "SlowTask.h"
#include "Kismet/GameplayStatics.h"

void UMainMenuUI::NativeConstruct()
{
	Super::NativeConstruct();

	if (ExitButton)
	{
		ExitButton->OnClicked.AddDynamic(this, &UMainMenuUI::OnExitButtonClicked);
	}
	
	UGameInstance* Instance = GetGameInstance();
	UMyGameInstanceSubsystem* MySubsystem = Instance->GetSubsystem<UMyGameInstanceSubsystem>();

	SNumeP = MySubsystem->GetUserInfo().user.name;
	UE_LOG(LogTemp, Warning, TEXT("ceva %s"), *SNumeP);
	TNumeProfesor = FText::FromString(SNumeP);
	NumeProfesor->SetText(TNumeProfesor);

	
	MySubsystem->StudentsReceived.AddUObject(this, &UMainMenuUI::GetStudentsComplete);
	
	MySubsystem->UpdateStudents();
	
}


void UMainMenuUI::GetStudentsComplete()
{
	UGameInstance* Instance = GetGameInstance();
	UMyGameInstanceSubsystem* MySubsystem = Instance->GetSubsystem<UMyGameInstanceSubsystem>();

	NumarElevi = MySubsystem->GetStudentInfo().users.Num();   //Cum aflu cati studenti sunt?

	//Setting Up the name of the Student
	for(int i=0; i<NumarElevi; i++)
	{
		NewButton.Add(Cast<UMainMenuButton>(CreateWidget(GetWorld(),WidgetClassI)));   //Aici vreau sa creez Widget nou. Asa ca am facut un alt widget separat cu Buton	
		SNumeE = MySubsystem->GetStudentInfo().users[0].name;
		SPrenumeE = MySubsystem->GetStudentInfo().users[0].surname;
		SFullNameE = FString::Printf(TEXT("%s %s"), *SNumeE, *SPrenumeE);
		TFullNameElev = FText::FromString(SFullNameE);
		
		FullNameElev.Add(NewButton[i]->StudentText);   // Pun numele studentlui (care e gol momentan) in fullnameelev
		
		NewButton[i]->StudentButton->AddChild(FullNameElev[i]);   //Adaug un TextBlock la butonul facut.
		FullNameElev[i]->SetText(TFullNameElev);
		ContainerButtons->AddChildToWrapBox(NewButton[i]);
	}

	for(int i=0; i<NumarElevi; i++)
	{
		if(NewButton[i])
		{
			NewButton[i]->StudentButton->OnClicked.AddDynamic(this, &UMainMenuUI::OnStudentButtonClicked);
		}
		
	}

}


void UMainMenuUI::OnExitButtonClicked()
{
	GetWorld()->GetFirstPlayerController()->ConsoleCommand("quit");
}

void UMainMenuUI::OnStudentButtonClicked()
{
	UGameInstance* Instance = GetGameInstance();
	UMyGameInstanceSubsystem* MySubsystem = Instance->GetSubsystem<UMyGameInstanceSubsystem>();

	MySubsystem->SavedElev = TFullNameElev;
	
	UGameplayStatics::OpenLevel(GetWorld(), "GameMenu");
}
