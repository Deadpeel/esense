// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Button.h"
#include "EditableTextBox.h"
#include "Image.h"
#include "TextBlock.h"
#include "Blueprint/UserWidget.h"
#include "LoginUI.generated.h"

/**
 * 
 */
UCLASS()

//class TMulticastDelegate

class ESENSE_API ULoginUI : public UUserWidget
{
	GENERATED_BODY()

private:
	virtual void NativeConstruct() override;

	UFUNCTION()
	void OnAutentificareButtonClicked();

	UFUNCTION()
	void OnExitButtonClicked();

	void AutentificareComplete(bool validCredentials);

private:

	UPROPERTY(meta=(BindWidget))
	UButton* Autentificare = nullptr;

	UPROPERTY(meta=(BindWidget))
	UButton* ExitButton = nullptr;

	UPROPERTY(meta=(BindWidget))
	UEditableTextBox* UsernameTextBox = nullptr;

	UPROPERTY(meta = (BindWidget))
	UEditableTextBox* PasswordTextBox = nullptr;

	UPROPERTY(meta = (BindWidget))
	UImage* Transparent = nullptr;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* Logging = nullptr;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* Incorrect = nullptr;
};
