// Fill out your copyright notice in the Description page of Project Settings.


#include "GamesUI.h"

#include "AudioMixer.h"
#include "ColorButton.h"
#include "GameWidget.h"
#include "GameSettingsSubsystem.h"
#include "WrapBox.h"
#include "Kismet/GameplayStatics.h"

void UGamesUI::NativeConstruct()
{
	Super::NativeConstruct();
	UGameWidget* GameWidget = CreateWidget<UGameWidget>(GetOwningPlayer(),GameWidgetClass);
	GameWidget->AddToViewport(100);
	
	if(CloseMenuButton)
	{
		CloseMenuButton->OnClicked.AddDynamic(this, &UGamesUI::OnCloseMenuButtonClicked);
	}

	if(ShapesButton)
	{
		ShapesButton->OnClicked.AddDynamic(this, &UGamesUI::OnShapesButtonClicked);
	}
	if(SettingButton)
	{
		SettingButton->OnClicked.AddDynamic(this, &UGamesUI::OnSettingsButtonClicked);
	}
	if(BgButton)
	{
		BgButton->OnClicked.AddDynamic(this, &UGamesUI::OnBgButtonClicked);
	}
	if(SoundButton)
	{
		SoundButton->OnClicked.AddDynamic(this, &UGamesUI::OnSoundButtonClicked);
	}
	if(ExitButton)
	{
		ExitButton->OnClicked.AddDynamic(this, &UGamesUI::OnExitButtonClicked);
	}

	if(Next)
	{
		Next->OnClicked.AddDynamic(this, &UGamesUI::OnNextButtonClicked);
	}
	if(Prev)
	{
		Prev->OnClicked.AddDynamic(this, &UGamesUI::OnPrevButtonClicked);
	}

	
	//*********Backgrounds*********

	
	if(BoxBackgrounds)
	{
		for(int i=0;i<BoxBackgrounds->GetChildrenCount();i++)
		{
				UColorButton* Color = Cast<UColorButton>(BoxBackgrounds->GetChildAt(i));
				Color->ColorReceived.AddUObject(this, &UGamesUI::OnColorButtonClicked);
		}
		
	}

	if(BoxButtons)
	{
		for(int i=0;i<BoxButtons->GetChildrenCount();i++)
		{
			UColorButton* ColorObject = Cast<UColorButton>(BoxButtons->GetChildAt(i));
			ColorObject->ColorReceived.AddUObject(this, &UGamesUI::OnShapeColorButtonClicked);
		}
		
	}

	if(ShapesBox)
	{
		for(int i=0;i<ShapesBox->GetChildrenCount();i++)
		{
			UColorButton* ColorObject = Cast<UColorButton>(ShapesBox->GetChildAt(i));
			ColorObject->TextureReceived.AddUObject(this, &UGamesUI::OnShapeButtonClicked);
		}
	}
}

//--------------------------Shapes-------------------------------

void UGamesUI::OnCloseMenuButtonClicked()
{
	CanvasPanelMenu->SetVisibility(ESlateVisibility::Hidden);
}

void UGamesUI::OnShapesButtonClicked()
{
	Switcher->SetActiveWidget(Shapes);
}
void UGamesUI::OnSettingsButtonClicked()
{
	Switcher->SetActiveWidget(Settings);
}
void UGamesUI::OnBgButtonClicked()
{
	Switcher->SetActiveWidget(Background);
}
void UGamesUI::OnSoundButtonClicked()
{
	//Sound off
}
void UGamesUI::OnExitButtonClicked()
{
	UGameplayStatics::OpenLevel(GetWorld(), "GameMenu");
}

// ------------------------------Settings------------------------------

void UGamesUI::OnNextButtonClicked()
{
	int index = SwitcherObjects->GetActiveWidgetIndex();
	index = index + 1;
	SwitcherObjects->SetActiveWidgetIndex(index);
	
}

void UGamesUI::OnPrevButtonClicked()
{
	int index = SwitcherObjects->GetActiveWidgetIndex();
	index = index - 1;
	SwitcherObjects->SetActiveWidgetIndex(index);
}

//---------------------------------Background---------------------------

void UGamesUI::OnColorButtonClicked(UButton* ClickedButton, FLinearColor ClickedColor)
{
	BgCustom->SetColorAndOpacity(ClickedColor);
	UGameSettingsSubsystem* MySettings = GetGameInstance()->GetSubsystem<UGameSettingsSubsystem>();
	MySettings->BackgroundColor = ClickedColor;
}


void UGamesUI::OnShapeColorButtonClicked(UButton* ClickedButton, FLinearColor ClickedColor)
{
	Thing->SetColorAndOpacity(ClickedColor);
	UGameSettingsSubsystem* MySettings = GetGameInstance()->GetSubsystem<UGameSettingsSubsystem>();
	MySettings->SelectedColor = ClickedColor;
}

void UGamesUI::OnShapeButtonClicked(UButton* ClickedButton, FSlateBrush ClickedBrush)
{
	Thing->SetBrush(ClickedBrush);
	SelectedShape->SetBrush(ClickedBrush);
	UGameSettingsSubsystem* MySettings = GetGameInstance()->GetSubsystem<UGameSettingsSubsystem>();
	MySettings->SelectedShape = ClickedBrush;
}

	//BgCustom->SetBrushFromTexture()
