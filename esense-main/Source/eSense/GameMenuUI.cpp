// Fill out your copyright notice in the Description page of Project Settings.


#include "GameMenuUI.h"
#include "MainMenuUI.h"
#include "MyGameInstanceSubsystem.h"
#include "Kismet/GameplayStatics.h"

void UGameMenuUI::NativeConstruct()
{
	Super::NativeConstruct();

	if (ExitButton)
	{
		ExitButton->OnClicked.AddDynamic(this, &UGameMenuUI::OnExitButtonClicked);
	}
	
	if (SchimbaElev)
	{
		SchimbaElev->OnClicked.AddDynamic(this, &UGameMenuUI::OnChangeStudentButtonClicked);
	}

	UGameInstance* Instance = GetGameInstance();
	UMyGameInstanceSubsystem* MySubsystem = Instance->GetSubsystem<UMyGameInstanceSubsystem>();
	
	NumeElev->SetText(MySubsystem->SavedElev);

	GetGamesComplete();
}

void UGameMenuUI::GetGamesComplete()
{
	for(int i=0; i<=3; i++)
	{
		GButton.Add(Cast<UGameMenuButton>(CreateWidget(GetWorld(),WidgetClassGame)));

		TGameName = FText::FromString(SGameName[i]);

		UE_LOG(LogTemp, Warning, TEXT("Cuvant %s"), *SGameName[i]);
		GameName.Add(GButton[i]->GameText);
		
		GButton[i]->GameButton->AddChild(GameName[i]);
		GameName[i]->SetText(TGameName);
		ContainerButtons->AddChildToWrapBox(GButton[i]);
	}
	
	for(int i=0; i<=3; i++)
	{
		if(GButton[i])
		{
			GButton[i]->GameButton->OnClicked.AddDynamic(this, &UGameMenuUI::OnGameButtonClicked);
		}	
	}
}

void UGameMenuUI::OnGameButtonClicked()
{
	UGameplayStatics::OpenLevel(GetWorld(), "Game");
}

void UGameMenuUI::OnExitButtonClicked()
{
	GetWorld()->GetFirstPlayerController()->ConsoleCommand("quit");
}

void UGameMenuUI::OnChangeStudentButtonClicked()
{
	UGameplayStatics::OpenLevel(GetWorld(), "MainMenu");
}