// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "BackgroundGame.generated.h"

/**
 * 
 */
UCLASS()
class ESENSE_API UBackgroundGame : public UUserWidget
{
	GENERATED_BODY()
	
};
