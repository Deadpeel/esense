// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Button.h"
#include "TextBlock.h"
#include "WrapBox.h"
#include "GameMenuButton.h"
#include "Blueprint/UserWidget.h"
#include "GameMenuUI.generated.h"

/**
 * 
 */
UCLASS()
class ESENSE_API UGameMenuUI : public UUserWidget
{
	GENERATED_BODY()

	private:

	virtual void NativeConstruct() override;

	UFUNCTION()
	void OnExitButtonClicked();

	UFUNCTION()
	void OnChangeStudentButtonClicked();

	UFUNCTION()
	void GetGamesComplete();
	
	UFUNCTION()
	void OnGameButtonClicked();
	
	
	private:

	UPROPERTY(meta=(BindWidget))
	UTextBlock* NumeElev = nullptr;
	
	UPROPERTY(meta=(BindWidget))
	UButton* ExitButton = nullptr;

	UPROPERTY(meta=(BindWidget))
	UButton* SchimbaElev = nullptr;

	UPROPERTY(meta=(BindWidget))
	UWrapBox* ContainerButtons = nullptr;
	
	UPROPERTY(EditAnywhere)
	TSubclassOf<UUserWidget> WidgetClassGame;

	UPROPERTY()
	TArray<UGameMenuButton*> GButton;

	UPROPERTY()
	TArray<UTextBlock*> GameName;

	TArray<FString> SGameName = {"Stimulare", "Localizare", "Identificare", "Discriminare"};
	FText TGameName;
};
