// Fill out your copyright notice in the Description page of Project Settings.


#include "ColorButton.h"

void UColorButton::NativeConstruct()
{
	Super::NativeConstruct();

	InnerButton->OnClicked.AddDynamic(this, &UColorButton::OnInnerTextureButtonClicked);
	InnerButton->OnClicked.AddDynamic(this, &UColorButton::OnInnerButtonClicked);
	
}

void UColorButton::NativePreConstruct()
{
	Super::NativePreConstruct();

	InnerImage->SetBrush(Brush);
	
}


void UColorButton::OnInnerButtonClicked()
{
	ColorReceived.Broadcast(InnerButton, ColorAndOpacity);
}

void UColorButton::OnInnerTextureButtonClicked()
{
	TextureReceived.Broadcast(InnerButton, Brush);
}
