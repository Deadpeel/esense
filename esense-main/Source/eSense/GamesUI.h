// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Button.h"
#include "CanvasPanel.h"
#include "GameWidget.h"
#include "HorizontalBox.h"
#include "Image.h"
#include "Overlay.h"
#include "Spacer.h"
#include "WidgetSwitcher.h"
#include "WrapBox.h"
#include "Blueprint/UserWidget.h"
#include "GamesUI.generated.h"

/**
 * 
 */
UCLASS()
class ESENSE_API UGamesUI : public UUserWidget
{
	GENERATED_BODY()

	private:
	
	virtual void NativeConstruct() override;

	UFUNCTION()
    void OnCloseMenuButtonClicked();

	//Shapes
	UFUNCTION()
	void OnShapesButtonClicked();
		UFUNCTION()
		void OnShapeColorButtonClicked(UButton* ClickedButton, FLinearColor ClickedColor);
	UFUNCTION()
		void OnShapeButtonClicked(UButton* ClickedButton, FSlateBrush ClickedBrush);
	
	//Settings
	UFUNCTION()
	void OnSettingsButtonClicked();
		UFUNCTION()
		void OnNextButtonClicked();
		UFUNCTION()
		void OnPrevButtonClicked();

	//Background
	UFUNCTION()
	void OnBgButtonClicked();
		UFUNCTION()
		void OnColorButtonClicked(UButton* ClickedButton, FLinearColor ClickedColor);
	
	UFUNCTION()
	void OnSoundButtonClicked();
	UFUNCTION()
	void OnExitButtonClicked();

	//StartGame
	//UFUNCTION()
	//void OnStartButtonClicked();

	
	UPROPERTY(meta=(BindWidget))
	UButton* CloseMenuButton = nullptr;

	UPROPERTY(meta=(BindWidget))
	UButton* ShapesButton = nullptr;
	UPROPERTY(meta=(BindWidget))
	UButton* SettingButton = nullptr;
	UPROPERTY(meta=(BindWidget))
    UButton* BgButton = nullptr;
	UPROPERTY(meta=(BindWidget))
	UButton* SoundButton = nullptr;
	UPROPERTY(meta=(BindWidget))
	UButton* ExitButton = nullptr;
	

	UPROPERTY(meta=(BindWidget))
	UHorizontalBox* CanvasPanelMenu = nullptr;

	//Shapes
	UPROPERTY(meta=(BindWidget))
	UWidgetSwitcher* Switcher = nullptr;
	
	UPROPERTY(meta=(BindWidget))
	UCanvasPanel* Shapes = nullptr;
	UPROPERTY(meta=(BindWidget))
	UCanvasPanel* Settings = nullptr;
	UPROPERTY(meta=(BindWidget))
	UCanvasPanel* Background = nullptr;

	UPROPERTY(meta=(BindWidget))
	UWrapBox* BoxButtons = nullptr;
	UPROPERTY(meta=(BindWidget))
	UWrapBox* ShapesBox = nullptr;

	UPROPERTY(meta=(BindWidget))
	UImage* SelectedShape = nullptr;
	

	//Settings
	UPROPERTY(meta=(BindWidget))
	UWidgetSwitcher* SwitcherObjects = nullptr;
	UPROPERTY(meta=(BindWidget))
	UButton* Next = nullptr;
	UPROPERTY(meta=(BindWidget))
	UButton* Prev = nullptr;

	//Background
	UPROPERTY(meta=(BindWidget))
	UWrapBox* BoxBackgrounds = nullptr;


	UPROPERTY(meta=(BindWidget))
	UImage* BgCustom = nullptr;

	UPROPERTY()
	UButton* ColorShape;
	UPROPERTY()
	UImage* CurrentBg;


	UPROPERTY(meta=(BindWidget))
	UOverlay* BackgroundsContainer;

	//Overall
	UPROPERTY(meta=(BindWidget))
	UImage* Thing = nullptr;
	UPROPERTY(meta=(BindWidget))
	USpacer* Space = nullptr;

	UPROPERTY(EditAnywhere)
	TSubclassOf<UGameWidget> GameWidgetClass;
};
