// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "IHttpRequest.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "MyGameInstanceSubsystem.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct FLoginPost
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly)
	FString email;

	UPROPERTY(BlueprintReadOnly)
	FString password;
	
};

USTRUCT(BlueprintType)
struct FInsideUser
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly)
	int id;

	UPROPERTY(BlueprintReadOnly)
	FString name;
	
	UPROPERTY(BlueprintReadOnly)
	FString surname;

	UPROPERTY(BlueprintReadOnly)
	FString email;

	UPROPERTY(BlueprintReadOnly)
	FString username;

	UPROPERTY(BlueprintReadOnly)
	FString type;

	UPROPERTY(BlueprintReadOnly)
	FString permissions;

	UPROPERTY(BlueprintReadOnly)
	int consent;
};


USTRUCT(BlueprintType)
struct FInsideStudent
{

	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly)
	int id;
	
	UPROPERTY(BlueprintReadOnly)
	FString name;
	
	UPROPERTY(BlueprintReadOnly)
	FString surname;
	
	UPROPERTY(BlueprintReadOnly)
	int hearing_impairment;
	
	UPROPERTY(BlueprintReadOnly)
	int visual_impairment;
	
	UPROPERTY(BlueprintReadOnly)
	FString birthdate;
	
	UPROPERTY(BlueprintReadOnly)
	FString gender;
	
	UPROPERTY(BlueprintReadOnly)
	FString created_at;
	
	UPROPERTY(BlueprintReadOnly)
	FString updated_at;
	
	UPROPERTY(BlueprintReadOnly)
	FString deleted_at;
	
	UPROPERTY(BlueprintReadOnly)
	int owner_id;
	
	UPROPERTY(BlueprintReadOnly)
	FString owner_type;
	
	UPROPERTY(BlueprintReadOnly)
	int archived;
	
	UPROPERTY(BlueprintReadOnly)
	int specialist_id;
	
	UPROPERTY(BlueprintReadOnly)
	int student_id;

};

USTRUCT(BlueprintType)
struct FMyResult
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly)
	FString token;

	UPROPERTY(BlueprintReadOnly)
	FInsideUser user;
};

USTRUCT(BlueprintType)
struct FStudentResult
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly)
	TArray<FInsideStudent> users;
};

UCLASS()
class ESENSE_API UMyGameInstanceSubsystem : public UGameInstanceSubsystem
{
public:
	GENERATED_BODY()
	void LoginWithCredentials(const FString& Username, const FString& Password);

	void UpdateStudents();
	
	DECLARE_MULTICAST_DELEGATE_OneParam(FIReceivedStuff, bool /*validCredentials*/);
	
	FIReceivedStuff IReceivedStuff;
	
	DECLARE_MULTICAST_DELEGATE(FStudentsReceived);

	FStudentsReceived StudentsReceived;

	const FMyResult& GetUserInfo() const;
	const FStudentResult& GetStudentInfo() const;

	FText SavedElev;
private:
	void OnResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
	void OnStudentsReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
	
	FMyResult Info;
	FStudentResult StudentInfo;
};
