// Fill out your copyright notice in the Description page of Project Settings.


#include "LoginUI.h"

#include "MyGameInstanceSubsystem.h"
#include "Components/Button.h"
#include "Engine/Engine.h"
#include "Kismet/GameplayStatics.h"

void ULoginUI::NativeConstruct()
{
	Super::NativeConstruct();

	Logging->SetVisibility(ESlateVisibility::Hidden);
	Transparent->SetVisibility(ESlateVisibility::Hidden);
	Incorrect->SetVisibility(ESlateVisibility::Hidden);

	if (ExitButton)
	{
		ExitButton->OnClicked.AddDynamic(this, &ULoginUI::OnExitButtonClicked);
	}

	if (Autentificare)
	{
		Autentificare->OnClicked.AddDynamic(this, &ULoginUI::ULoginUI::OnAutentificareButtonClicked);
	}
}

void ULoginUI::OnAutentificareButtonClicked()
{
	UGameInstance* GameInstance = GetGameInstance();
	UMyGameInstanceSubsystem* MySubsystem = GameInstance->GetSubsystem<UMyGameInstanceSubsystem>();

	FString Username = UsernameTextBox->GetText().ToString();
	FString Password = PasswordTextBox->GetText().ToString();
	MySubsystem->LoginWithCredentials(Username, Password);
	MySubsystem->IReceivedStuff.AddUObject(this, &ULoginUI::AutentificareComplete);

	Logging->SetVisibility(ESlateVisibility::Visible);
	Transparent->SetVisibility(ESlateVisibility::Visible);
}

void ULoginUI::OnExitButtonClicked()
{
	GetWorld()->GetFirstPlayerController()->ConsoleCommand("quit");
}

void ULoginUI::AutentificareComplete(bool validCredentials)
{
	UGameInstance* GameInstance = GetGameInstance();
	UMyGameInstanceSubsystem* MySubsystem = GameInstance->GetSubsystem<UMyGameInstanceSubsystem>();
	
	if (!validCredentials)
	{
		Logging->SetVisibility(ESlateVisibility::Hidden);
		Transparent->SetVisibility(ESlateVisibility::Hidden);
		Incorrect->SetVisibility(ESlateVisibility::Visible);
	}
	
	if(validCredentials)
	{
		UGameplayStatics::OpenLevel(GetWorld(), "MainMenu");
	}
}
