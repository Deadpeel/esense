// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Button.h"
#include "MainMenuButton.h"
#include "ScaleBox.h"
#include "SizeBox.h"
#include "TextBlock.h"
#include "WrapBox.h"
#include "Blueprint/UserWidget.h"
#include "MainMenuUI.generated.h"

/**
 * 
 */
UCLASS()
class ESENSE_API UMainMenuUI : public UUserWidget
{
	GENERATED_BODY()


private:

	virtual void NativeConstruct() override;

	UFUNCTION()
	void OnExitButtonClicked();

	UFUNCTION()
	void OnStudentButtonClicked();
	
	void GetStudentsComplete();

	

protected:

	//UPROPERTY(EditAnywhere,Category="Class Types")
	//TSubclassOf<UUserWidget> WidgetClass;
	
private:

	UPROPERTY(meta=(BindWidget))
	UTextBlock* NumeProfesor = nullptr;

	UPROPERTY(meta=(BindWidget))
	UButton* ExitButton = nullptr;

	UPROPERTY(meta=(BindWidget))
	UWrapBox* ContainerButtons = nullptr;

	UPROPERTY()
	UScaleBox* Scale;
	
	UPROPERTY(EditAnywhere)
	TSubclassOf<UUserWidget> WidgetClassI;

	UPROPERTY()
	TArray<UMainMenuButton*> NewButton;

	UPROPERTY()
	TArray<UTextBlock*> FullNameElev;
	
	FString SNumeP;
	FText TNumeProfesor;

	FString SNumeE;
	
	FString SPrenumeE;

	FString SFullNameE;
	FText TFullNameElev;

	int NumarElevi;
};
