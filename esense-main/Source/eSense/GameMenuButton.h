// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Button.h"
#include "ScaleBox.h"
#include "TextBlock.h"
#include "GameMenuButton.generated.h"

/**
 * 
 */
UCLASS()
class ESENSE_API UGameMenuButton : public UUserWidget
{
	GENERATED_BODY()

	public:
	
	UPROPERTY(meta=(BindWidget))
	UButton* GameButton = nullptr;

	UPROPERTY(meta=(BindWidget))
	UTextBlock* GameText = nullptr;

	UPROPERTY(meta=(BindWidget))
	UScaleBox* GameScale = nullptr;
};
